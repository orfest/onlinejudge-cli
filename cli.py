#!/usr/bin/python3
"""Processes user queries."""

import bs4
import datetime
import getpass
import http.cookiejar
import json
import logging
import os
import re
import shutil
import socket
import sys
import tempfile
import time
import urllib.error
import urllib.parse
import urllib.request

import problemset


class Cli(object):
  """This class processes user queries."""

  def __init__(self):
    Cli.AssureDirectoryExists(self._data_directory)
    self._all_problems = self._LoadCache()
    self._LoadCookies()
    self._LoadSecretData()
    self._easiest_filter_problem_arguments = [
        '--no-solved', '--sort-authors', '--reverse', '--limit=30']

  def _LoadCookies(self):
    self._InitCookieJar()
    if not os.access(self._cookies_file, os.R_OK):
      print('Warning: cookies not found')
    else:
      self._cookiejar.load(ignore_discard=True)

  def _InitCookieJar(self):
    self._cookiejar = http.cookiejar.LWPCookieJar(
        self._cookies_file, None,
        http.cookiejar.DefaultCookiePolicy(None, None, True, False))
    cookieprocessor = urllib.request.HTTPCookieProcessor(self._cookiejar)
    opener = urllib.request.build_opener(
        urllib.request.HTTPRedirectHandler, cookieprocessor)

    urllib.request.install_opener(opener)

  def _LoadCache(self):
    if os.access(self._problems_cache_file, os.R_OK):
      return problemset.ProblemSet(
          text=open(self._problems_cache_file, 'r').read())
    else:
      print('Warning: problems cache not found. Use fetch_list to create it')
    return problemset.ProblemSet()

  def _SaveProblemsCache(self):
    open(self._problems_cache_file, 'w').write(self._all_problems.ToString())

  def _UpdateCache(self, problem_id, solved):
    problems = [p for p in self._all_problems.GetProblems()
                if p.GetId() == problem_id]
    if not problems:
      print('ERROR: problem is not found in the problems cache, '
            'forcing fetch_list')
      return self.FetchList([])
    problem = problems[0]
    if not problem.IsSolved():
      problem.SetSolved(solved)
    self._SaveProblemsCache()

  def _ReadUrl(self, url, data=None, data_no_encoding=None, headers_dict=None):
    print("Reading URL %s" % url)
    data_to_use = None
    if data is not None:
      data_to_use = urllib.parse.urlencode(data).encode('ascii')
    if data_no_encoding is not None:
      data_to_use = data_no_encoding
    if not headers_dict:
      headers_dict = {}
    headers_dict['User-Agent'] = (
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)'
        ' Chrome/28.0.1500.71 Safari/537.36')
    attempt = 0
    while True:
      attempt += 1
      assert attempt < 20
      try:
        req = urllib.request.Request(url, data_to_use, headers=headers_dict)
        return urllib.request.urlopen(req, timeout=10.0).read()
      except (urllib.error.HTTPError, urllib.error.URLError) as error:
        logging.error('Data not retrieved because %s\nURL: %s', error, url)
      except socket.timeout:
        logging.error('Socket timed out - URL %s', url)
      else:
        logging.info('Access successful.')

  def _CheckHtml2Text(self):
    return os.system('html2text -version >/dev/null') == 0

  def _RenderHtml(self, html):
    if not self._CheckHtml2Text():
      return 'ERROR: html2text not found'

    inputname = self._SaveTextToTmpfile(html)
    outputname = tempfile.mktemp('', 'tmp', None)
    errorlogname = tempfile.mktemp('', 'tmp', None)

    invokestr = 'html2text -style pretty -o %s <%s >%s' % (
        outputname, inputname, errorlogname)
    retcode = os.system(invokestr)
    if retcode != 0:
      return 'ERROR: render failed:\n%s'%open(errorlogname, 'rb').read()

    output = open(outputname, 'rb')
    text = output.read()
    return text

  def _ResolveProblemId(self, problem_id):
    if problem_id == '--easiest':
      return self._GetEasiestProblem()
    if problem_id == '--relaxed':
      return self._GetRelaxedProblem()
    else:
      return problem_id

  def _ResolveProblemIdFromArgs(self, args):
    if '--easiest' in args:
      return self._GetEasiestProblem()
    elif '--relaxed' in args:
      return self._GetRelaxedProblem()
    return None

  def _SaveTextToTmpfile(self, text):
    tmpname = tempfile.mktemp('', 'gygy', None)
    tmpfile = open(tmpname, 'wb')
    tmpfile.write(text)
    tmpfile.close()
    return tmpname

  def _FixHtml(self, html):
    html = b''.join(html.split(b'\r\n'))
    html = b''.join(html.split(b'\n'))
    html = re.sub(br'(onclick|onmouseover)="[^"]*"', b'', html)
    html = re.sub(b'<p<', b'<p><', html)
    html = re.sub(b'<ul<', b'<ul><', html)
    html = re.sub(b'<!--(.*?)-->', b'', html)
    return html

  def _SaveSecretData(self, secret_data):
    open(self._secret_data_file, 'w').write(json.dumps(secret_data))

  def _FilterProblems(self, arguments):
    if self._all_problems is None or not self._all_problems.GetProblems():
      print('Warning: problems list empty')
      self.FetchList()

    problems = problemset.ProblemSet(self._all_problems)
    for arg in arguments:
      if arg.find('--limit=') == 0:
        problems.SetLimit(int(arg[len('--limit='):]))
      else:
        {
            '--solved': lambda x: x.FilterSolved(True),
            '--failed': lambda x: x.FilterSolved(False),
            '--untried': lambda x: x.FilterSolved(None),
            '--no-solved': lambda x: x.RemoveSolved(True),
            '--no-failed': lambda x: x.RemoveSolved(False),
            '--no-untried': lambda x: x.RemoveSolved(None),
            '--sort-authors': lambda x: x.SortAuthors(),
            '--sort-difficulty': lambda x: x.SortDifficulty(),
            '--sort-dirt': lambda x: x.SortAccepted(),
            '--sort-id': lambda x: x.SortId(),
            '--sort-solved': lambda x: x.SortSolved(),
            '--sort-source': lambda x: x.SortSource(),
            '--sort-title': lambda x: x.SortTitle(),
            '--reverse': lambda x: x.Reverse(),
        }[arg](problems)
    return problems

  def _GetLastSubmission(self, problem_id):
    submissions = self._GetSubmissions()
    if not submissions:
      return None
    for s in submissions:
      if s.GetProblemId() == problem_id:
        return s
    return None

  def _DoLogin(self, password):
    url = self._GetAuthUrl()
    s = self._ReadUrl(url)
    additional_login_data = self._GetAdditionalLoginData(s)
    self._cookiejar.save(ignore_discard=True)
    data = self._BuildLoginData(password, additional_login_data)
    s = self._ReadUrl(url, data=data)
    self._cookiejar.save(ignore_discard=True)
    assert self._LoginSuccessful(s), 'Login failed'
    secret_data = self._BuildSecretData(password, additional_login_data)
    self._SaveSecretData(secret_data)

  def _GetEasiestProblem(self):
    problems = self._FilterProblems(
        self._easiest_filter_problem_arguments).GetProblems()
    if not problems:
      return None
    return problems[0].GetId()

  def _GetRelaxedProblem(self):
    return self._FilterProblems('--relaxed')[0].id

  def _GetLanguage(self, filename):
    extension = os.path.splitext(filename)[1]
    extensions_map = self._BuildExtensionMap()
    if extension not in extensions_map:
      logging.critical('Unknown extension %s', extension)
    return extensions_map[extension]

  def _Unescape(self, l):
    l = re.sub('&gt;', '>', l)
    l = re.sub('&lt;', '<', l)
    l = re.sub('&quot;', '"', l)
    l = re.sub('&nbsp;', '"', l)
    return l

  def _PrintDfs(self, l):
    if type(l) == bs4.element.NavigableString:
      return self._Unescape(str(l))
    else:
      c = list(l.children)
      stringlist = []
      for i in c:
        stringlist.append(self._PrintDfs(i))
      if l.name == 'p' or l.name == 'li' or l.name == 'i' or (
          l.name == 'span' and l.get('class')[0] in [
              'tex-span', 'tex-font-style-it', 'tex-font-style-tt']):
        return ''.join(stringlist)
      elif l.name == 'sub':
        return '_'+' '.join(stringlist)
      elif l.name == 'sup':
        return '^'+' '.join(stringlist)
      else:
        return '\n'.join(stringlist)

  def _GetSolutionFilepath(self, full_problem_id, args):
    (contest_id, problem_id) = self._SplitProblemId(full_problem_id)
    path = self._name
    filename = full_problem_id
    if "--contest-dir" in args:
      path += "/%s" % contest_id
      filename = problem_id
    if "--write-blank=rust" in args:
      filepath = "%s/%s.rs" % (path, filename)
    elif "--write-blank=cpp" in args:
      filepath = "%s/%s.cpp" % (path, filename)
    else:
      raise ValueError("Unknown language: %s" % str(args))
    return filepath

  def _PrepareBlank(self, full_problem_id, args):
    filepath = self._GetSolutionFilepath(full_problem_id, args)
    path = os.path.dirname(filepath)
    if "--create-dir" in args:
      Cli.AssureDirectoryExists(path)
    if os.path.exists(filepath):
      print("Not preparing %s" % filepath)
    else:
      if "--write-blank=rust" in args:
        shutil.copyfile("%s/blanks/blank.rs" % self._base_data_directory, filepath)
      elif "--write-blank=cpp" in args:
        shutil.copyfile("%s/blanks/blank.cpp" % self._base_data_directory, filepath)
      print("Prepared %s" % filepath)

  def Submit(self, args):
    """Submits given file as a solution of a given problem."""
    serious_args = [arg for arg in args if not arg.startswith('--')]
    problem_id = str(self._ResolveProblemIdFromArgs(args[0]))
    used_serious_args = 0
    if not problem_id:
      if len(serious_args) < 1:
        raise ValueError("No problem_id: %s" % str(args))
      problem_id = serious_args[0]
      used_serious_args += 1
    solution_file_name = None
    solution_file = None
    if len(serious_args) >= 1+used_serious_args:
      solution_file_name = args[used_serious_args]
      used_serious_args += 1
    else:
      solution_file_name = self._GetSolutionFilepath(problem_id, args)
    solution_file = open(solution_file_name, 'r')
    solution = solution_file.read()
    language = self._GetLanguage(solution_file_name)
    filename = os.path.basename(solution_file_name)

    if self._DoSubmit(language, problem_id, solution, filename) is None:
      logging.critical('Submit failed. Problem: %s, Filename: %s',
                       problem_id, filename)

    while True:
      my_submission = self._GetLastSubmission(problem_id)
      print(str(my_submission))
      if my_submission and my_submission.IsFinalVerdict():
        break
      time.sleep(1)

    self._UpdateCache(my_submission.problem, my_submission.IsSolved())

  def Fetch(self, args):
    """Fetches problem statement of a given problem."""
    if len(args) < 1:
      self.Help()
      return
    problem_id = self._ResolveProblemId(args[0])
    print("Fetching problem %s" % problem_id)
    url = self._GetProblemUrl(problem_id)
    html = self._ReadUrl(url)
    html = self._FixHtml(html)
    soup = bs4.BeautifulSoup(html, "lxml")
    divs = [d for d in soup.find_all('div')
            if d.get('class') and d.get('class')[0] == 'problem-statement']
    if not divs:
      logging.critical('Bad problem statement page.')
    sys.stdout.buffer.write((self._PrintDfs(divs[0])+'\n').encode('utf-8'))
    print("Fetched problem %s" % problem_id)
    self._ParseSampleTests(soup)
    self._PrepareBlank(problem_id, args)

  def Login(self):
    self._cookiejar.clear()
    print('Enter judgeid: ')
    self._judge_id = input()
    password = getpass.getpass('Enter password: ')
    self._DoLogin(password)

  def Relogin(self):
    self._cookiejar.clear()
    secrets = self._LoadSecretData()
    password = secrets['password']
    self._DoLogin(password)

  def Status(self, args=None):
    assert args and len(args) >= 1
    submission_ids = set(args)
    for submission in self._GetSubmissions():
      if submission.id in submission_ids:
        print(str(submission))

  def Ls(self, arguments):
    print('Ls')
    if arguments.__contains__('--easiest'):
      self._FilterProblems(self._easiest_filter_problem_arguments).Render()
    else:
      self._FilterProblems(arguments).Render()

  def FetchList(self):
    print('FetchList')
    self._all_problems = self._DoFetchList()
    self._all_problems.Dedup()
    self._SaveProblemsCache()

  def Submissions(self):
    for submission in self._GetSubmissions():
      print(str(submission))

  def Skip(self, args=None):
    for p in args:
      problem_id = str(self._ResolveProblemId(p))
      self._UpdateCache(problem_id, True)

  def Chart(self):
    accepted_timestamps = self._GetChartData()
    print(self._GetChartUrl(accepted_timestamps))

  def _GetPerDayStats(self, accepted_timestamps):
    today = datetime.date.today()
    stats = {}
    for i in accepted_timestamps:
      t = datetime.datetime.fromtimestamp(i)
      delta = today - t.date()
      d = delta.days
      if d not in stats:
        stats[d] = 0
      stats[d] += 1
    return stats

  def _GetChartUrl(self, accepted_timestamps):
    per_day_stats = self._GetPerDayStats(accepted_timestamps)
    s = 0
    days_to_draw = 100
    for i in range(days_to_draw):
      v = 0
      if i in per_day_stats:
        v = per_day_stats[i]
      s += v
    chd_values = []
    for i in range(days_to_draw):
      chd_values.append(str(s))
      if i in per_day_stats:
        s -= per_day_stats[i]
    chd_str = 'chd=t:%s' % ','.join(chd_values)
    chds_str = 'chds=a'
    cht_str = 'cht=lc'
    chs_str = 'chs=1000x1000'
    chxt_str = 'chxt=x,y'
    attr_str = [chd_str, chds_str, chs_str, cht_str, chxt_str]
    return 'https://chart.googleapis.com/chart?%s' % '&'.join(attr_str)

  def ContestStats(self):
    problems = self._all_problems.GetProblems()
    stats = {}
    for p in problems:
      s = p.GetSource()
      if s not in stats:
        stats[s] = [0, 0]
      stats[s][1] += 1
      if p.IsSolved():
        stats[s][0] += 1
    rev_stats = {}
    for s in stats.keys():
      v = (stats[s][0], stats[s][1])
      if v not in rev_stats:
        rev_stats[v] = 1
      else:
        rev_stats[v] += 1
    rev_rev_stats = []
    for v in rev_stats.keys():
      rev_rev_stats.append((-rev_stats[v], v[0], v[1]))
    rev_rev_stats.sort()
    print("Stats:")
    for v in rev_rev_stats:
      print("%d/%d: %d" % (v[1], v[2], -v[0]))

  @staticmethod
  def AssureDirectoryExists(directory):
    if not os.access(directory, os.F_OK):
      os.makedirs(directory, 0o700)

  def Help(self):
    """Displays help."""
    help_str = 'commands:\n'
    help_str += 'submit <problem number> <solution file>\n'
    help_str += 'relogin\n'
    help_str += 'fetch_list\n'
    help_str += 'ls <arguments>\n'
    help_str += 'fetch <id>\n'
    help_str += 'submissions\n'
    help_str += 'clear\n'
    help_str += 'status <submission_id>\n'
    help_str += 'fetch_solution <num/--all> [--save] [--slow=LIMIT]\n'
    help_str += 'problem_statistics <num1> <num2> ...\n'
    help_str += 'skip <num1> <num2> ...\n'
    help_str += 'chart\n'
    help_str += 'contest_stats\n'
    print(help_str)
