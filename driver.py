#!/usr/bin/python3

"""Onlinejudge CLI entry point. Dispatches calls."""

import os
import sys

import codeforces


def Init(exe_path):
  archive_name = os.path.basename(exe_path)
  if archive_name == 'codeforces':
    return codeforces.CodeforcesCli()
  return None


if __name__ == '__main__':
  cli_module = Init(sys.argv[0])
  if cli_module is None:
    sys.exit(0)
  if len(sys.argv) < 2:
    cli_module.Help()
    sys.exit(0)
  command = sys.argv[1]
  args = sys.argv[2:]
  if command == 'submit':
    cli_module.Submit(args)
  elif command == 'fetch':
    cli_module.Fetch(args)
  elif command == 'status':
    cli_module.Status(args)
  elif command == 'fetch_list':
    cli_module.FetchList()
  elif command == 'login':
    cli_module.Login()
  elif command == 'ls':
    print('ls')
    cli_module.Ls(args)
  elif command == 'submissions':
    cli_module.Submissions()
  elif command == 'relogin':
    cli_module.Relogin()
  elif command == 'skip':
    cli_module.Skip(args)
  elif command == 'chart':
    cli_module.Chart()
  elif command == 'contest_stats':
    cli_module.ContestStats()
  else:
    cli_module.Help()
