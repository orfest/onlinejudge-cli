"""Encodes data to for a submit."""

import mimetypes


class Submitter(object):
  """Encodes data into a submittable format."""

  @staticmethod
  def EncodeMultipartFormdata(fields, files):
    """Converts files and fields into a multipart format."""
    boundary = '----WebKitFormBoundary7J4lfsyilines9gCjC61'
    crlf = '\r\n'
    lines = []
    for (key, value) in fields:
      lines.append('--' + boundary)
      lines.append('Content-Disposition: form-data; name="%s"' % key)
      lines.append('')
      lines.append(value)

    for (key, filename, value) in files:
      lines.append('--' + boundary)
      lines.append(
          'Content-Disposition: form-data; name="%s"; filename="%s"' % (
              key, filename))
      lines.append('Content-Type: %s' % Submitter._GetContentType(filename))
      lines.append('')
      lines.append(value)

    lines.append('--' + boundary + '--')
    lines.append('')
    body = crlf.join(lines)
    content_type = 'multipart/form-data; boundary=%s' % boundary
    return content_type, body.encode('ascii')

  @staticmethod
  def _GetContentType(filename):
    return mimetypes.guess_type(filename)[0] or 'application/octet-stream'

