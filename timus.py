import http.cookiejar
import os
import re
import tempfile
import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse

from Cli import Cli
from Problem import Problem
from ProblemSet import ProblemSet
from Submitter import Submitter
import bs4
from Submission import Submission

class TimusCli(Cli):

    def __init__(self):
        print("TimusCli.__init__")
        self._name = 'timus'
        self._baseurl = 'http://acm.timus.ru/'
        self._base_data_directory = '%s/.onlinejudge-cli'%os.environ.get('HOME')
        self._data_directory = '%s/timus-data'%self._base_data_directory
        print(('data_directory = %s'%self._data_directory))
        self._problems_cache_file = '%s/problems_cache.txt'%self._data_directory
        self._secret_data_file = '%s/secret.txt'%self._data_directory
        self._cookies_file = '%s/cookies.txt'%self._data_directory
        Cli.__init__(self)

    def _load_secret_data(self):
        if not os.access(self._secret_data_file, os.R_OK):
            print('Warning: secret data file not found')
            self.login()
        secret_raw_data = open(self._secret_data_file, 'r').read()
        secrets = eval(secret_raw_data)
        self._judge_id = secrets['AuthorID']

    def _get_submit_url(self):
        return '%s/submit.aspx'%self._baseurl

    def _get_short_judge_id(self):
        return '80862'

    def _get_judge_id(self):
        return '80862VM'

    def _get_submissions_url(self):
        return '%s/status.aspx?space=1&author=%s&count=%s'%(self._baseurl, self._get_short_judge_id(), 1000)

    def _parse_submission(self, tr):
        alltds = tr('td')
        submission = Submission()
        submission.id = alltds[0].getText()
        submission.date = alltds[1].getText()
        submission.author_id = alltds[2]('a')[0]['href'].split('=')[1]
        submission.author = alltds[2].getText()
        submission.problem = alltds[3].getText()[:4]
        submission.language = alltds[4].getText()
        submission.judgement_result = alltds[5].getText()
        submission.test = alltds[6].getText()
        submission.execution_time = alltds[7].getText()
        submission.memory_used = alltds[8].getText()
        return submission

    def _get_submissions(self):
        url = self._get_submissions_url(problem_id)
        html = self._read_url(url)
        soup = bs4.BeautifulSoup(html, "lxml")
        submission_tr = [tr for tr in soup('tr') if ['even', 'odd'].count(tr.get('class')) > 0]
        print(submission_tr)
        submissions = []
        for tr in submission_tr:
            submissions.append(self._parse_submission(tr))
        return submissions


    def _get_problem_url(self, problem_id):
        return '%s/print.aspx?space=1&num=%s'%(self._baseurl, problem_id)

    def _valid_problemset_page(self, page_contents):
        if page_contents.find('Access to the problem set is denied') >= 0:
            return False
        return page_contents.find('Every problem has a simple, fast and wrong solution.') < 0

    def _get_problemset_url(self, page):
        return '%s/problemset.aspx?space=1&page=%d'%(self._baseurl, page)

    def _do_fetch_list(self):
        problems = ProblemSet()
        page = 1
        while True:
            print(('Fetching page %d'%page))
            url = self._get_problemset_url(page)
            page_contents = self._read_url(url)
            if not self._valid_problemset_page(page_contents):
                break
            page_problems = self._parse_problems(page_contents)
            problems.problems.extend(page_problems.problems)
            page += 1
        return problems

    def _parse_single_problem(self, tr):
        alltds = tr('td')
        if len(alltds) != 6 or 'class' not in alltds[2]:
            return None
        id = alltds[1].string
        res = Problem()
        if len(alltds[0]('img')) == 1: # + or - in 'solved' column
            img = alltds[0]('img')[0]
            src = img['src']
            res.solved = src.find('ok') >= 0;
        res.id = id
        name = alltds[2]('a')[0].string
        res.title = name
        contest = alltds[3].string
        res.source = contest
        difficulty = int(alltds[5].string)
        res.accepted_procent = 1
        res.difficulty = difficulty
        authors = alltds[4]('a')
        if len(authors) > 0:
            res.authors = int(authors[0].string)
        else:
            res.authors = 0
        return res

    def _parse_problems(self, html):
        allproblems = ProblemSet()
        html = self._fix_html(html)
        soup = bs4.BeautifulSoup(html, "lxml")
        for tr in soup('tr'):
            problem = self._parse_single_problem(tr)
            if problem is not None:
                allproblems.problems.append(problem)
        return allproblems

    def _get_auth_url(self):
        return '%s/authedit.aspx'%self._baseurl

    def _build_login_data(self, password):
        return {'Action':'edit', 'JudgeId':self._judge_id, 'Password':password}

    def _build_secret_data(self,password):
        return {'AuthorID':self._judge_id}

    def _build_extension_map(self):
        return {'.cpp':'10', '.java':'7'}

    def _do_submit(self, language, problem_id, solution, filename):
        content_type, data = self._get_submit_data(language, problem_id, solution, filename)
        url = self._get_submit_url()
        req = urllib.request.Request(url, data,{'Content-Type':content_type})
        r = urllib.request.urlopen(req)
        s = r.read()
        return True

    def _get_submit_data(self, language, problem_id, solution, filename):
        fields = [
            ('Action', 'submit'),
            ('JudgeID', self._get_judge_id()),
            ('Language', language),
            ('ProblemNum', problem_id),
            ('SpaceID', '1'),
            ('Source', '')
        ]
        files = [('SourceFile', filename, solution)]
        return Submitter.encode_multipart_formdata(fields, files)
