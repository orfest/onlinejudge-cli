#!/usr/bin/python3
"""Collection of problems."""

import json

from problem import Problem


class ProblemSet(object):
  """This class manages collection of problems."""

  def __init__(self, copy_from=None, text=None):
    if copy_from:
      self._limit = copy_from.GetLimit()
      self._problems = list(copy_from.GetProblems())
    elif text is None:
      self._limit = None
      self._problems = []
    else:
      self._limit = None
      problems_list = json.loads(text)
      self._problems = [Problem(from_dict=problem_dict)
                        for problem_dict in problems_list]

  def GetLimit(self):
    return self._limit

  def GetProblems(self):
    return self._problems

  def AddProblem(self, p):
    self._problems.append(p)

  def AddProblemSet(self, ps):
    self._problems.extend(ps.GetProblems())

  def Render(self):
    problems_to_output = self._problems
    if self._limit is not None:
      problems_to_output = problems_to_output[:self._limit]
    for p in problems_to_output:
      p.Render()
    print("shown problems: %d of %d\n" % (
        len(problems_to_output), len(self._problems)))

  def Dedup(self):
    self.SortId()
    problems = []
    for i in range(len(self._problems)):
      if i == 0 or self._problems[i].GetId() != self._problems[i-1].GetId():
        problems.append(self._problems[i])
    self._problems = problems

  def ToString(self):
    problems = [p.ToDict() for p in self._problems]
    return json.dumps(problems)

  def RemoveSolved(self, v):
    self._problems = [p for p in self._problems if p.IsSolved() != v]

  def FilterSolved(self, v):
    self._problems = [p for p in self._problems if p.IsSolved() == v]

  def Reverse(self):
    self._problems.reverse()

  def SetLimit(self, lim):
    self._limit = lim

  def SortAuthors(self):
    self._problems.sort(key=lambda p: p.NumAuthors())

  def SortSolved(self):
    self._problems.sort(key=lambda p: str(p.IsSolved()))

  def SortId(self):
    self._problems.sort(key=lambda p: p.GetId())

  def SortTitle(self):
    self._problems.sort(key=lambda p: p.GetTitle())

  def SortSource(self):
    self._problems.sort(key=lambda p: p.GetSource())

  def SortDifficulty(self):
    self._problems.sort(key=lambda p: -int(p.GetDifficulty()))

  def SortAccepted(self):
    self._problems.sort(key=lambda p: p.GetAcceptedProcent())
