#!/usr/bin/python3
"""Represents a problem state."""

import sys


class Problem(object):
  """Represents state of a problem."""

  def __init__(self, from_dict=None):
    self._solved = None
    self._id = None
    self._title = None
    self._source = None
    self._accepted_procent = None
    self._authors = None
    self._difficulty = None
    if from_dict:
      if 'solved' in from_dict:
        self._solved = from_dict['solved']
      if 'id' in from_dict:
        self._id = from_dict['id']
      if 'title' in from_dict:
        self._title = from_dict['title']
      if 'source' in from_dict:
        self._source = from_dict['source']
      if 'accepted_procent' in from_dict:
        self._accepted_procent = from_dict['accepted_procent']
      if 'authors' in from_dict:
        self._authors = from_dict['authors']
      if 'difficulty' in from_dict:
        self._difficulty = from_dict['difficulty']

  def ToDict(self):
    """Converts state of a problem into a dictionary."""
    d = {}
    if self._solved is not None:
      d['solved'] = self._solved
    if self._id is not None:
      d['id'] = self._id
    if self._title is not None:
      d['title'] = self._title
    if self._source is not None:
      d['source'] = self._source
    if self._accepted_procent is not None:
      d['accepted_procent'] = self._accepted_procent
    if self._authors is not None:
      d['authors'] = self._authors
    if self._difficulty is not None:
      d['difficulty'] = self._difficulty
    return d

  def GetId(self):
    return self._id

  def GetAcceptedProcent(self):
    return self._accepted_procent

  def GetDifficulty(self):
    return self._difficulty

  def GetSource(self):
    return self._source

  def GetTitle(self):
    return self._title

  def IsSolved(self):
    return self._solved

  def NumAuthors(self):
    return self._authors

  def SetSolved(self, solved):
    self._solved = solved

  def _ToStringSolved(self, solved):
    return {None: '', False: '-', True: '+'}[solved].ljust(2)

  def _ToStringStr(self, string, length):
    if string is None:
      string = ''
    if len(string) > length:
      string = string[:length]
    return string.ljust(length)

  def Render(self):
    line = (self._ToStringSolved(self._solved)+
            self._ToStringStr(str(self._id), 6)+
            self._ToStringStr(self._title, 40)+
            self._ToStringStr(self._source, 40)+
            self._ToStringStr(str(self._authors), 10)+
            self._ToStringStr(str(self._difficulty), 6)+
            '\n')
    sys.stdout.buffer.write(line.encode('utf-8'))

  def __cmp__(self, another):
    if self.GetId() < another.GetId():
      return -1
    elif self.GetId() > another.GetId():
      return 1
    else:
      return 0
