#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <algorithm>
#include <iomanip>
#include <cassert>
#include <string>
#include <cstring>
#include <cstdio>
// #include <cmath>

using namespace std;

#if ORF_AT_HOME
#define Eo(x) { cerr << #x << " = " << (x) << endl; }
#else
#define Eo(x) {}
#endif

using int64 = long long;

template <typename A, typename B>
ostream& operator<<(ostream& os, const pair<A,B>& v) {
  return os << "(" << v.first << "," << v.second << ")";
}

template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
  os << "{";
  for (const auto& x : v) {
    os << x << ",";
  }
  return os << "}";
}

template <typename T>
ostream& operator<<(ostream& os, const set<T>& v) {
  os << "{";
  for (const auto& x : v) {
    os << x << ",";
  }
  return os << "}";
}

template <typename T>
ostream& operator<<(ostream& os, const unordered_set<T>& v) {
  os << "{";
  for (const auto& x : v) {
    os << x << ",";
  }
  return os << "}";
}

template <typename A, typename B>
ostream& operator<<(ostream& os, const map<A,B>& v) {
  os << "{";
  for (const auto& x : v) {
    os << x << ";";
  }
  return os << "}";
}

template <typename A, typename B>
ostream& operator<<(ostream& os, const unordered_map<A,B>& v) {
  os << "{";
  for (const auto& x : v) {
    os << x << ";";
  }
  return os << "}";
}

int main() {
  return 0;
}
