"""Represents submission state."""

import datetime


class Submission(object):
  """Represents submission's state."""

  def __init__(self):
    self.id = None
    self.date = None
    self.author = None
    self.author_id = None
    self.problem = None
    self.problem_title = None
    self.language = None
    self.judgement_result = None
    self.test = None
    self.execution_time = None
    self.memory_used = None
    self.timestamp = None
    self._final_verdicts = ['idleness limit', 'compilation error',
                            'wrong answer', 'runtime error', 'accepted',
                            'timelimit exceeded']

  def __str__(self):
    return (self._Tostr(self.id, 8) +
            self._Tostr(self.GetDate(), 20) +
            self._Tostr(self.author_id, 8) +
            self._Tostr(self.problem, 6) +
            self._Tostr(self.problem_title, 50) +
            self._Tostr(self.language, 10) +
            self._Tostr(self.judgement_result, 16) +
            self._Tostr(self.test, 4) +
            self._Tostr(self.execution_time, 8) +
            self._Tostr(self.memory_used, 6))

  def GetDate(self):
    assert self.timestamp
    return datetime.datetime.fromtimestamp(self.timestamp).strftime(
        '%H:%M:%S %Y.%m.%d')

  def _Tostr(self, string, length):
    if string is None:
      string = ''
    if len(string) > length:
      string = string[:length]
    return string.ljust(length)

  def IsFinalVerdict(self):
    judge = self.judgement_result.lower()
    if not judge:
      return False
    for s in self._final_verdicts:
      if judge.startswith(s):
        return True
    return False

  def GetProblemId(self):
    return self.problem

  def IsSolved(self):
    return self.judgement_result.startswith('accepted')
