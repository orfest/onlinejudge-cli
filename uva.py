import os
import urllib.request, urllib.error, urllib.parse
import re

from Cli import Cli
from Problem import Problem
from ProblemSet import ProblemSet
from Submission import Submission
from Submitter import Submitter
import bs4

class UvaCli(Cli):

    def __init__(self):
        self._name = 'uva'
        self._baseurl = 'http://uva.onlinejudge.org/'
        self._base_data_directory = '%s/.onlinejudge-cli'%os.environ.get('HOME')
        self._data_directory = '%s/uva-data'%self._base_data_directory
        self._problems_cache_file = '%s/problems_cache.txt'%self._data_directory
        self._secret_data_file = '%s/secret.txt'%self._data_directory
        self._cookies_file = '%s/cookies.txt'%self._data_directory
        Cli.__init__(self)

    def _load_secret_data(self):
        if not os.access(self._secret_data_file, os.R_OK):
            print('Warning: secret data file not found')
            self.login()
        secret_raw_data = open(self._secret_data_file, 'r').read()
        secrets = eval(secret_raw_data)
        self._judge_id = secrets['username']
        return secrets

    def _get_auth_url(self):
        return "%s/index.php?option=com_comprofiler&task=login"%self._baseurl

    def _build_login_data(self, password):
        url = self._baseurl
        s = self._read_url(url)

        data = {'username':self._judge_id, 'passwd':password, 'submit':'Login'}
        all_fields = re.findall('<input type="hidden" name="\w*" value="[:_a-z0-9A-Z]*" />', s)[1:9]
        for field in all_fields:
            name = re.search('(?<=name=")\w*', field).group(0)
            value = re.search('(?<=value=")[a-zA-Z0-9:_]*', field).group(0)
            data[name] = value
        #data['force_session'] = '0'
        return data

    def _build_secret_data(self, password):
        return {'username':self._judge_id, 'password':password}

    def _do_fetch_list(self):
        problemset = self._do_recursive_fetch('index.php?option=com_onlinejudge&Itemid=8', 'all')
        return problemset

    def _do_recursive_fetch(self, page_url, source):
        print(source)
        problemset = ProblemSet()
        page_html = self._read_url('%s/%s'%(self._baseurl, page_url))
        volume_entry_list = self._parse_volume_page(page_html, source)
        for entry in volume_entry_list:
            if entry[0] == 'FOLDER':
                problemset.add_problemset(self._do_recursive_fetch(entry[1], entry[2]))
            else:
                problemset.add_problem(self._parse_single_problem(entry, source))
        return problemset

    def _parse_volume_page(self, page_html, source):
        html = self._fix_html(page_html)
        soup = bs4.BeautifulSoup(html, "lxml")
        entries = []
        for tr in soup('tr'):
            solved = None
            authors = None
            accepted_procent = None
            tr_class = tr.get('class')
            tr_style = tr.get('style')
            good = False
            if tr_class is not None and (tr_class == 'sectiontableentry1' or tr_class == 'sectiontableentry2'):
                good = True
            if tr_style is not None and tr_style.find('background-color:#cc') == 0:
                good = True
            if good is False:
                continue

            if tr_style is not None and tr_style.find('#ccccff') >= 0:
                solved = False
            elif tr_style is not None and tr_style.find('#ccffcc') >= 0:
                solved = True

            tds = tr('td')
            img1 = tds[0]('img')[0]
            icon_type = img1.get('alt')
            link_td = tds[1]
            a2 = link_td('a')[0]
            href = a2.get('href')
            name = a2.string
            td2 = tds[2].string
            td3 = None
            if len(tds[3]('div')) >= 1:
                td3 = tds[3]('div')[0]('div')[1].string
            td4 = tds[4].string
            td5 = None
            if len(tds[3]('div')) >= 1:
                td5 = tds[5]('div')[0]('div')[1].string
            entries.append((icon_type, href, name, solved, td2, td3, td4, td5))
        return entries

    def _parse_single_problem(self, entry, source):
        title = entry[2]
        p = title.find(' ')
        id = None
        if p >= 0:
            id = title[:p]
            while True:
                if title[p] != ' ' and title[p] != '-':
                    break
                p += 1
            title = title[p:]
        authors_procent = 0
        if entry[7][0] != 'N':
            authors_procent = float(entry[7][:-1])
        authors_tried = int(entry[6])
        authors = int(round(authors_tried*authors_procent*0.01))
        accepted_procent = 0
        if entry[5][0] != 'N':
            accepted_procent = float(entry[5][:-1])
        problem = Problem(tuple=(entry[3], id, title, source, int(round(accepted_procent)), authors))
        return problem

    def _get_problem_url(self, problem_id):
        id = int(problem_id)
        vol = id/100
        return '%s/external/%d/%d.html'%(self._baseurl, vol, id)

    def _build_extension_map(self):
        return {'.c':'1', '.cpp':'3', '.java':'2'}

    def _do_submit(self, language, problem_id, solution, filename):
        content_type, data = self._get_submit_data(language, problem_id, solution, filename)
        url = self._get_submit_url()
        req = urllib.request.Request(url, data,{'Content-Type':content_type})
        r = urllib.request.urlopen(req)
        s = r.read()
        return True

    def _get_submit_data(self, language, problem_id, solution, filename):
        fields = [
            ('problemid', ''),
            ('category', ''),
            ('localid', problem_id),
            ('language', language),
            ('code', '')
        ]
        files = [('codeupl', filename, solution)]
        return Submitter.encode_multipart_formdata(fields, files)

    def _get_submit_url(self):
        return '%sindex.php?option=com_onlinejudge&Itemid=25&page=save_submission'%self._baseurl

    def _get_submissions_url(self):
        return '%s/index.php?option=com_onlinejudge&Itemid=9'%self._baseurl

    def _get_submissions(self, problem_id=None):
        url = self._get_submissions_url()
        html = self._read_url(url)
        soup = bs4.BeautifulSoup(html, "lxml")
        submission_tr = [tr for tr in soup('tr') if ['sectiontableentry1', 'sectiontableentry2'].count(tr.get('class')) > 0]
        submissions = []
        for tr in submission_tr:
            submissions.append(self._parse_submission(tr))
        if problem_id is not None:
            return submissions
            submissions = [p for p in submissions if p.problem == problem_id]
        return submissions

    def _extract_id(self, title):
        p = title.find(' ')
        if p < 0:
            return title
        return title[:p]

    def _parse_submission(self, tr):
        alltds = tr('td')
        submission = Submission()
        submission.id = alltds[0].getText()
        submission.date = alltds[6].getText()
        submission.author_id = ''
        submission.author = ''
        submission.problem = alltds[1].getText() 
        submission.problem_title = alltds[2].getText() 
        submission.language = alltds[4].getText()
        submission.judgement_result = alltds[3].getText()
        submission.test = ''
        submission.execution_time = alltds[5].getText()
        submission.memory_used = ''
        return submission
