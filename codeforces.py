"""Codeforces-specific logic."""
import bs4
import datetime
import json
import logging
import math
import os
import re
import shutil
import time

import cli
import problem
import problemset
import submission
import submitter


class CodeforcesCli(cli.Cli):
  """Manages Codeforces-specific logic."""

  def __init__(self):
    self._name = 'codeforces'
    self._baseurl = 'http://codeforces.com'
    self._base_data_directory = '%s/.onlinejudge-cli'%os.environ.get('HOME')
    self._data_directory = '%s/codeforces-data'%self._base_data_directory
    self._problems_cache_file = '%s/problems_cache.txt'%self._data_directory
    self._secret_data_file = '%s/secret.txt'%self._data_directory
    self._cookies_file = '%s/cookies.txt'%self._data_directory
    cli.Cli.__init__(self)

  def _LoadSecretData(self):
    if not os.access(self._secret_data_file, os.R_OK):
      print('Warning: secret data file not found: %s' % self._secret_data_file)
      self.Login()
    secret_raw_data = open(self._secret_data_file, 'r').read()
    secrets = json.loads(secret_raw_data)
    assert 'Handle' in secrets
    assert 'password' in secrets
    assert 'csrf_token' in secrets
    self._judge_id = secrets['Handle']
    self._csrf_token = secrets['csrf_token']
    return secrets

  def _GetAuthUrl(self):
    return '%s/enter' % self._baseurl

  def _ComputeSignForms(self, seed):
    tmp = 0
    for i in range(len(seed)):
      tmp += (i+1) * (i+2) * ord(seed[i:i+1])
      tmp %= 1009
      if i%3 == 0:
        tmp += 1
      if i%2 == 0:
        tmp *= 2
      if i > 0:
        z = math.floor(i/2)
        tmp -= math.floor(ord(seed[z:z+1])/2) * (tmp%5)
      while tmp < 0:
        tmp += 1009
      while tmp >= 1009:
        tmp -= 1009
    return tmp

  def _GetMagicCookie(self):
    c = open(self._cookies_file, 'rb')
    cookies = c.read()
    c.close()
    marker = b'39ce7='
    k = cookies.find(marker)
    if k < 0:
      logging.critical('Magic cookie not found.')
    rest = cookies[k+len(marker):]
    p = rest.find(b';')
    cookie_value = rest[:p]
    return cookie_value

  def _GetAdditionalLoginData(self, login_page_html):
    soup = bs4.BeautifulSoup(login_page_html, "lxml")
    body = soup.body
    assert body
    csrf_span = body.find_all('span')[0]
    csrf = csrf_span['data-csrf']
    assert csrf
    return {'csrf_token': csrf}

  def _BuildLoginData(self, password, additional_login_data):
    assert 'csrf_token' in additional_login_data
    cookie_value = self._GetMagicCookie()
    return {'handleOrEmail': self._judge_id,
            'password': password,
            'remember': 'on',
            '_tta': self._ComputeSignForms(cookie_value),
            'csrf_token': additional_login_data['csrf_token'],
            'action': 'enter'}

  def _BuildSecretData(self, password, additional_login_data):
    assert 'csrf_token' in additional_login_data
    return {'Handle': self._judge_id,
            'password': password,
            'csrf_token': additional_login_data['csrf_token']}

  def _GetProblemsetUrl(self, page):
    return '%s/problemset/page/%d'%(self._baseurl, page)

  def _HasNext(self, html):
    html = self._FixHtml(html)
    soup = bs4.BeautifulSoup(html, "lxml")
    divs = [d for d in soup('div')
            if d.get('class') and d.get('class')[0] == 'pagination']
    li = divs[0]('ul')[0].contents
    return li[len(li)-2].name != 'span'

  def _ParseSingleProblem(self, tr):
    alltds = tr('td')
    if len(alltds) != 5 or alltds[0].get('class')[0] != 'id':
      return None
    problem_id = alltds[0]('a')[0].string.strip()
    title = alltds[1]('div')[0]('a')[0].string.strip()
    source = self._GetContestId(problem_id)
    authors = 0
    if len(alltds) >= 5 and alltds[4] and alltds[4]('a'):
      authors = int(alltds[4]('a')[0].contents[1][2:])
    difficulty = 0
    if len(alltds) >= 4 and alltds[3] and alltds[3]('span'):
      span = alltds[3]('span')[0]
      difficulty = int(span.contents[0])
    solved = None
    if tr.get('class') is not None and tr.get('class')[0] == 'accepted-problem':
      solved = True
    elif tr.get('class') and tr.get('class')[0] == 'rejected-problem':
      solved = False
    return problem.Problem(from_dict={
        'id': problem_id,
        'title': title,
        'source': source,
        'authors': authors,
        'difficulty': difficulty,
        'solved': solved})

  def _ParseSample(self, in_or_out_div):
    pres = in_or_out_div('pre')
    if pres:
      return pres[0].get_text('\n')
    return None

  def _ParseSampleTests(self, soup):
    sample_tests = soup.find_all("div", class_="sample-test")
    if sample_tests and len(sample_tests) == 1:
      sample_test = sample_tests[0]
      sample_test_inputs = sample_test.find_all("div", class_="input")
      sample_test_outputs = sample_test.find_all("div", class_="output")
      if os.path.exists('tests'):
        shutil.rmtree('tests')
      cli.Cli.AssureDirectoryExists('tests')
      if len(sample_test_inputs) == len(sample_test_outputs):
        num = 0
        for t in zip(sample_test_inputs, sample_test_outputs):
          num += 1
          input_text = self._ParseSample(t[0])
          output_text = self._ParseSample(t[1])
          print("input: %s" % input_text)
          print("output: %s" % output_text) 
          in_name = "tests/%d" % num
          with open(in_name, "w") as f:
            f.write(input_text)
            print("Wrote %s" % in_name)
          out_name = "tests/%d.out" % num
          with open(out_name, "w") as f:
            f.write(output_text)
            print("Wrote %s" % out_name)

  def _ParseProblems(self, html):
    allproblems = problemset.ProblemSet()
    html = self._FixHtml(html)
    soup = bs4.BeautifulSoup(html, "lxml")
    for tr in soup('tr'):
      if not tr.find_all('td'):
        continue
      p = self._ParseSingleProblem(tr)
      if p:
        allproblems.AddProblem(p)
    return allproblems

  def _DoFetchList(self):
    problems = problemset.ProblemSet()
    page = 1
    has_next = True
    while has_next:
      print('Fetching page %d'%page)
      url = self._GetProblemsetUrl(page)
      page_contents = self._ReadUrl(url)
      page_problems = self._ParseProblems(page_contents)
      problems.AddProblemSet(page_problems)
      page += 1
      has_next = self._HasNext(page_contents)
    print("Fetched %d problems" % len(problems.GetProblems()))
    return problems

  def _GetSubmitUrl(self, csrf_token=None):
    if csrf_token:
      return '%s/problemset/submit?csrf_token=%s' % (self._baseurl, csrf_token)
    else:
      return '%s/problemset/submit' % self._baseurl

  def _BuildExtensionMap(self):
    return {'.cpp': '54', '.cc': '54', '.c': '43', '.py': '31', '.rs': '49', '.hs': '12'}

  def _GetSubmitData(self, language, problem_id, solution, filename):
    assert self._csrf_token
    fields = [
        ('csrf_token', self._csrf_token),
        ('action', 'submitSolutionFormSubmitted'),
        ('contestId', problem_id[:-1]),
        ('submittedProblemIndex', problem_id[-1:]),
        ('programTypeId', language),
        ('source', ''),
        ('_tta', str(self._ComputeSignForms(self._GetMagicCookie()))),
    ]
    files = [('sourceFile', filename, solution)]
    return submitter.Submitter.EncodeMultipartFormdata(fields, files)

  def _DoSubmit(self, language, problem_id, solution, filename):
    url = self._GetSubmitUrl()
    s = self._ReadUrl(url)
    self._csrf_token = self._GetAdditionalLoginData(s)['csrf_token']

    content_type, data = self._GetSubmitData(
        language, problem_id, solution, filename)
    url = self._GetSubmitUrl(csrf_token=self._csrf_token)
    s = self._ReadUrl(url, data_no_encoding=data,
                      headers_dict={'Content-Type': content_type})
    return True

  def _LoginSuccessful(self, html):
    html = self._FixHtml(html)
    soup = bs4.BeautifulSoup(html, "lxml")
    logout = 0
    enter = 0
    for a in soup('a'):
      href = a['href']
      if href.endswith('logout'):
        logout += 1
      if href.endswith('enter'):
        enter += 1
    assert (enter == 0 and logout > 0) or (enter > 0 and logout == 0)
    return logout > 0

  def _SplitProblemId(self, problem_id):
    l = len(problem_id)
    for i in range(l):
      if not problem_id[i].isdigit():
        return (problem_id[:i], problem_id[i:])
    return (problem_id[:-2], problem_id[-2:])

  def _GetContestId(self, problem_id):
    split_problem_id = self._SplitProblemId(problem_id)
    return split_problem_id[0] if split_problem_id else None

  def _GetProblemId(self, problem_id):
    split_problem_id = self._SplitProblemId(problem_id)
    return split_problem_id[1] if split_problem_id else None

  def _GetProblemUrl(self, problem_id):
    return '%s/problemset/problem/%s/%s' % (self._baseurl,
                                            self._GetContestId(problem_id),
                                            self._GetProblemId(problem_id))

  def _GetSubmissionsUrl(self, page=None):
    if not page:
      return '%s/submissions/%s' % (self._baseurl, self._judge_id)
    return '%s/submissions/%s/page/%d' % (self._baseurl, self._judge_id, page)

  def _GetSubmissions(self):
    url = self._GetSubmissionsUrl()
    html = self._ReadUrl(url)
    html = self._FixHtml(html)
    soup = bs4.BeautifulSoup(html, "lxml")
    submission_tr = [tr for tr in soup('tr') if tr.get('data-submission-id')]
    submissions = []
    for tr in submission_tr:
      submissions.append(self._ParseSubmission(tr))
    return submissions

  def _ExtractNumber(self, status):
    return int((re.findall(r'(\d+)', status))[0])

  def _ParseJudgementResult(self, status):
    if status.startswith('Running on test'):
      return 'running', self._ExtractNumber(status)
    elif status.startswith('Accepted') or status.startswith('С Новым годом!') or status.startswith('Happy New Year!'):
      return 'accepted', 0
    elif status.startswith('Wrong answer on test'):
      return 'wrong answer', self._ExtractNumber(status)
    elif status.startswith('Runtime error on test'):
      return 'runtime error', self._ExtractNumber(status)
    elif status.startswith('Time limit exceeded'):
      return 'timelimit exceeded', self._ExtractNumber(status)
    elif status.startswith('Compilation error'):
      return 'compilation error', 0
    elif status.startswith('Idleness limit'):
      return 'idleness limit', self._ExtractNumber(status)
    else:
      return status, -1

  def _GetProblemIdFromProblemUrl(self, url):
    parts = url.split('/')
    return parts[2] + parts[4]

  def _ParseSubmission(self, tr):
    alltds = tr('td')
    s = submission.Submission()
    s.id = tr.get('data-submission-id')
    date = alltds[1].getText().strip()
    s.timestamp = time.mktime(
        datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S').timetuple())
    s.author_id = alltds[2]('a')[0].getText().strip()
    s.author = alltds[2]('a')[0].getText().strip()
    s.problem = self._GetProblemIdFromProblemUrl(alltds[3]('a')[0]['href'])
    s.problem_title = alltds[3].getText().strip()
    p = s.problem_title.find('-')
    s.problem_title = s.problem_title[p+2:]
    s.language = alltds[4].getText().strip()
    s.judgement_result, s.test = self._ParseJudgementResult(
        alltds[5].getText().strip())
    s.test = str(s.test)
    s.execution_time = alltds[6].getText().strip()
    s.memory_used = alltds[7].getText().strip()
    return s

  def _HasNextSubmissionsPage(self, html):
    html = self._FixHtml(html)
    soup = bs4.BeautifulSoup(html, "lxml")
    divs = [d for d in soup.find_all('div')
            if d.get('class') and len(d.get('class')) == 1 and
            d.get('class')[0] == 'pagination']
    all_ul = divs[0].find_all('ul')
    assert all_ul
    ul = all_ul[0]
    is_last_span = False
    for j in ul:
      if type(j) == bs4.element.NavigableString:
        continue
      if j.name == 'span':
        is_last_span = True
      else:
        is_last_span = False
    return not is_last_span

  def _GetAllAcceptedSubmissions(self):
    page = 1
    has_next = True
    submissions = []
    while has_next:
      print('Fetching page %d' % page)
      url = self._GetSubmissionsUrl(page=page)
      html = self._ReadUrl(url)
      html = self._FixHtml(html)
      soup = bs4.BeautifulSoup(html, "lxml")
      submission_tr = [tr for tr in soup('tr') if tr.get('data-submission-id')]
      for tr in submission_tr:
        s = self._ParseSubmission(tr)
        if s.IsSolved():
          submissions.append(s)
      page += 1
      has_next = self._HasNextSubmissionsPage(html)
    return submissions

  def _GetChartData(self):
    submissions = self._GetAllAcceptedSubmissions()
    first_accepted = {}
    for s in submissions:
      p = s.problem
      if p in first_accepted and first_accepted[p] > s.timestamp:
        first_accepted[p] = s.timestamp
      elif p not in first_accepted:
        first_accepted[p] = s.timestamp
    accepted_timestamps = list(first_accepted.values())
    accepted_timestamps.sort()
    return accepted_timestamps

